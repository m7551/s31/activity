const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

// Export the model in order to use it in other files
module.exports = mongoose.model("Task", taskSchema);