const express = require('express');
const router = express.Router();

// Import taskController to use its functions
const taskController = require("../controllers/taskController.js");

// Retrieving all tasks
router.get('/', (req, res) => {
	taskController.getAllTask().then(resultFromController => {
		res.send(resultFromController);
	})
});


// Creating a task
// This route expects to receive POST request at the URL "/tasks/"
router.post('/', (req, res) => {
	console.log(req.body)
	taskController.createTask(req.body).then(resultFromController => {
		res.send(resultFromController)
	})
})

// Deleting a task
router.delete('/:id', (req, res) => {
	console.log(req.params)
	taskController.deleteTask(req.params.id).then(resultFromController => {
		res.send(resultFromController);
	})
})

// Updating a task
router.put('/:id', (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => {
		res.send(resultFromController);
	})
})

// --------------------------- Activity ---------------------------
// Retrieving a specific task
router.get('/:id', (req, res) => {
	taskController.specificTask(req.params.id).then(resultFromController => {
		res.send(resultFromController);
	})
})

// Updating a completed task
router.put('/:id/complete', (req, res) => {
	taskController.completedTask(req.params.id).then(resultFromController => {
		res.send(resultFromController);
	})
})

// --------------------------- Activity End -----------------------

// Exports the module so it can be import to other files
module.exports = router;