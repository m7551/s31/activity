const express = require('express');
const mongoose = require('mongoose');

// Importing the taskRoute to use its function
const taskRoute = require("./routes/taskRoutes.js");


const port = process.env.PORT || 4000;
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database Connection
mongoose.connect("mongodb+srv://admin:admin123@course-booking.jmriv.mongodb.net/B157_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

let db = mongoose.connection;
db.once('open', () => {console.log('MongoDB Database is established.')})


// Add the task route
//Allow all the task routes create in the "taskRoute.js" file to use the "/tasks" route
app.use("/tasks", taskRoute); //http:localhost:4000/tasks/

app.listen(port, () => {
	console.log(`Server is currently running on port: ${port}`)
})