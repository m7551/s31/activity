// Controllers contain the functions and business logic of our Expressjs application

const Task = require('../models/task');

// Controller function for getting all the task
// Defines the functions to be used in the "taskRoute.js" and exports these functions
module.exports.getAllTask = () => {
	// model.method
	return Task.find({}).then(result => {return result})
}

// Controller function for creating a task
// The request body coming from the client was passed from the 'taskRoute.js' via the 'req.body' as an argument and was renamed as a 'requestBody' parameter in the controller file
module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name: requestBody.name
	})

	// Saves the newly created "newTask" object in database
	// .then waits until the task is stored in the database or an error is encountered before returning a true or false value back to the client
	return newTask.save().then((task, err)=> {
		if(err) {
			console.log(err);
			return false;
		} else {
			return task;
		}
	})
}

// Controller function for deleting a task
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if (err){
			console.log(err);
			return false;
		} else {
			return removedTask
		}
	})
} 

// Controller function for updating a task
module.exports.updateTask = (taskId, newContent) => {
	return Task.findByIdAndUpdate(taskId).then((result, err) => {
		if (err){
			console.log(err)
			return false;
		}
		result.name = newContent.name
		return result.save().then((updatedTask, err) => {
			if(err){
				console.log(err)
				return false;
			} else {
				return updatedTask;
			}
		})
	}) 
}

// --------------------------- Activity ---------------------------

// Controller function for retrieving a specific task
module.exports.specificTask = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if (err){
			console.log(err)
			return false
		} else {
			return result;
		}
	})
}

// Controller function for updating a completed task
module.exports.completedTask = (taskId, completeStatus) => {
	return Task.findByIdAndUpdate(taskId).then((result, err) => {
		if (err){
			console.log(err)
		}
		result.status = "complete"
		return result.save()
		.then((updatedTask, err) => {
			if (err){
				console.log(err)
			} else {
				return updatedTask;
			}
		})
	})
}

// --------------------------- Activity End -----------------------